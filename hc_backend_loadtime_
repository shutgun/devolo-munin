#!/bin/bash

# Get host to query
HOST=${0##*hc_backend_loadtime_}
if [[ ! $HOST ]]; then
	>&2 echo "This is a wildcard plugin. Please specify a devolo Home Control Central Unit by linking hc_backend_loadtime_<device> to this file."
	exit 1
fi

# Check, if jq is installed
if ! hash jq 2>/dev/null; then
	>&2 echo "This plugin requires jq."
	exit 2
fi

# Check, if netcat is installed
NCAT=${netcat:-$(which nc)}
if ! hash $NCAT 2>/dev/null; then
	>&2 echo "This plugin requires netcat."
	exit 2
fi

# Query host
SERVERURL=`curl --silent -G --data-urlencode "version=1" --data-urlencode "target=PTS" --data-urlencode "key=Baptization.mPRMServerURL" $HOST:22879 | jq -r ".result"`
if [[ ! $SERVERURL ]] || [[ $SERVERURL == "malformed_request" ]]; then
        >&2 echo "You are most likely not querying a Home Control Central Unit."
        exit 3
fi

LOCATION=${SERVERURL#*://}
LOCATION=${LOCATION%:*}
PORT=${SERVERURL##*:}

# Generate config
case $1 in
        config)
                echo "host_name $HOST"
                echo "graph_title Backend load time of Home Control"
                echo "graph_vlabel Load time in seconds"
                echo "graph_category network"
                echo "graph_args --base 1000 -l 0"
                echo "graph_info This graph shows the load time in seconds"
                echo "load.label $LOCATION"
                echo "load.type GAUGE"
                echo "load.min 0"
        exit 0;;
esac

# Generate data
LC_ALL="C"
TIMEFORMAT="%3R"
DURATION=$((time $NCAT --ssl -z $LOCATION $PORT) 2>&1)
echo "load.value $DURATION"
