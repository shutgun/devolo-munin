# Munin plugins for devolo devices

These plugins bring various statistics for devolo devices to Munin.

## delos_datarates_

This wildcard plugin shows PLC data rates of a delos powered device. At time of writing, this were:

 * Magic 1 WiFi
 * Magic 2 WiFi

It needs [jq](https://stedolan.github.io/jq/) to interpret the data. Since it cannot be run on the delos device itself, you should add a host configuration in `/etc/munin/munin.conf` like the following, if you want the device to appear as own host in Munin.

    [<hostname>]
        address 127.0.0.1
        use_node_name no

## dhci_datarates_

This wildcard plugin shows PLC data rates of a dHCI capable device. At time of writing, this were:

 * dLAN 500 AV Wireless+
 * dLAN 500 WiFi
 * dLAN 550+ WiFi
 * dLAN WiFi outdoor
 * dLAN TV SAT Multituner
 * dLAN 1000+ WiFi ac
 * dLAN 1200+ WiFi ac
 * dLAN 1200+ WiFi n
 * Home Control

It needs [jq](https://stedolan.github.io/jq/) to interpret the data. Since it cannot be run on the dHCI device itself, you should add a host configuration in `/etc/munin/munin.conf` like the following, if you want the device to appear as own host in Munin.

    [<hostname>]
        address 127.0.0.1
        use_node_name no

## hc_backend_loadtime_

This wildcard plugin shows the load time of the backend of a devolo Home Control Central Unit.

It needs [jq](https://stedolan.github.io/jq/) and netcat to interpret the data. Since it cannot be run on the Home Control Central Unit itself, you should add a host configuration in `/etc/munin/munin.conf` like the following, if you want the device to appear as own host in Munin.

    [<hostname>]
        address 127.0.0.1
        use_node_name no

You can configure the netcat binary by adding a config block to your plugin configuration in `/etc/munin/plugin-conf.d/`

    [hc_backend_loadtime_*]
        env.netcat /usr/bin/ncat/ncat
